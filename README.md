# pixelfoo-viewer

[<img alt="codeberg" src="https://img.shields.io/badge/codeberg-poc/pixelfoo-viewer?style=for-the-badge&labelColor=555555&logo=codeberg" height="20">](https://codeberg.org/poc/pixelfoo-viewer)
[![Crate](https://img.shields.io/crates/v/pixelfoo-viewer.svg)](https://crates.io/crates/pixelfoo-viewer)

> A viewer for pixelfoo frame streams.

## Background

At Chaospott in Essen we have a large 80x40 array of Neopixel LEDs
in the ceiling of our main room.
A server can run various apps to generate animations on the LED array.

## Apps

Apps can be written in any programming language,
they are started as child processes.
They get the width and height as the first and second command line parameter,
plus currently one additional free-to-use third parameter.

Apps generate a stream of fixed format pixel data as their standard output.
A pixel is either three bytes for red, green and blue (RGB),
or four bytes with additionally white (RGBW).
The number of pixels specified by the width make up a row,
and the number of rows specified by the height make up a frame.
Frames are sent consecutively, without any additional delimiters.

The app controls the speed by the timing of the frame data it sends.

## Viewer

The viewer runs such apps locally and displays the animation in a window.
This helps with app development.

For example,
```
pixelfoo-viewer -w apps/backlight.py 80 40
```
runs the `backlight.py` app (assuming it exists) in RGBW mode
with an 80x40 matrix.

## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
