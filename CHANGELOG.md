# Change Log

## [Unreleased] - [unreleased]

### Breaking Changes

### Additions

### Changes

### Fixes

### Thanks

## 0.1.1 - 2023-04-16

### Changes
- Added shields to README.
- Fixed some typos.

## 0.1.0 - 2023-04-16

Initial release.

